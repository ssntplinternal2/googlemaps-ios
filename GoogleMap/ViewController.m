//
//  ViewController.m
//  GoogleMap
//
//  Created by Sword Software on 29/07/19.
//  Copyright © 2019 Sword Software. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    CLLocationManager *mgr;
    
}
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    mgr = [[CLLocationManager alloc]init];
    [mgr requestWhenInUseAuthorization];
    [_mapView setMapType:MKMapTypeSatelliteFlyover];
}



- (IBAction)selectMapType:(id)sender {
    
    switch (((UISegmentedControl *)sender).selectedSegmentIndex) {
        case 0:
            _mapView.mapType = MKMapTypeStandard;
            break;
        case 1:
            _mapView.mapType = MKMapTypeSatellite;
            break;
        case 2:
            _mapView.mapType = MKMapTypeHybrid;
            break;
        default:
            break;
    }
}



-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    _mapView.centerCoordinate = userLocation.location.coordinate;
    
    [_mapView setRegion:MKCoordinateRegionMakeWithDistance(userLocation.location.coordinate, 800, 800)animated:YES];
    MKPointAnnotation *point = [[MKPointAnnotation alloc]init];
    point.coordinate = userLocation.location.coordinate;
    point.title = @"this is my area";
    point.subtitle = @"Apple HeadQuarters";
    
    [_mapView addAnnotation:point];
    
}


-(void)btnMethod{
    NSLog(@"the button is pressed");
}

-(void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

@end
