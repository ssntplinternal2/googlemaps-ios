//
//  ViewController.h
//  GoogleMap
//
//  Created by Sword Software on 29/07/19.
//  Copyright © 2019 Sword Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ViewController : UIViewController <MKMapViewDelegate>


@end

